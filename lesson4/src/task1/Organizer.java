package task1;

import java.time.LocalDateTime;
import java.util.*;

public class Organizer implements TaskManager {

    private Map<LocalDateTime, List<Task>> toDoList;

    public Organizer() {
        toDoList = new TreeMap<LocalDateTime, List<Task>>();
    }

    public Map<LocalDateTime, List<Task>> getToDoList() {
        return toDoList;
    }

    @Override
    public void add(LocalDateTime date, Task task) {
        List<Task> taskList = this.toDoList.get(date);

        if (taskList == null) {
            taskList = new ArrayList<Task>();
            toDoList.put(date, taskList);
        }

        taskList.add(task);
    }

    @Override
    public void remove(LocalDateTime date) {
        this.toDoList.remove(date);
    }

    @Override
    public Set<String> getCategories() {
        Set<String> categories = new HashSet<String>();

        for (List<Task> tasks : toDoList.values()) {
            for (Task task : tasks) {
                categories.add(task.getCategory());
            }
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories(String... categories) {
        Map<String, List<Task>> tasksByCategories = new HashMap<String, List<Task>>();

        for (String category : categories) {
            tasksByCategories.put(category, getTasksByCategory(category));
        }
        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> taskList = new ArrayList<Task>();

        for (List<Task> tasks : toDoList.values()) {
            for (Task task : tasks) {
                if (task.getCategory().equals(category)) {
                    taskList.add(task);
                }
            }
        }
        return taskList;
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> taskList = new ArrayList<Task>();
        LocalDateTime  today  = LocalDateTime.now();

        for (LocalDateTime date : toDoList.keySet()) {
            if ((date.toLocalDate()).equals(today.toLocalDate())) {
                taskList.addAll(toDoList.get(date));
            }
        }
        return taskList;
    }
}
