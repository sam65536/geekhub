package task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TooLongWords {

    public static void main(String[] args) throws IOException {
        int numberOfWords;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print("Enter the number of words: ");
            try {
                numberOfWords = Integer.parseInt(reader.readLine());
                if ((numberOfWords >= 1) && (numberOfWords <= 100)) {
                    break;
                }
                else {
                    System.out.println("Incorrect number!");
                }
            } catch (Exception e) {
                System.out.println("Incorrect number!");
            }
        }

        String[] words = new String[numberOfWords];
        System.out.println("Enter " + numberOfWords + " words:");

        for (int i = 0; i < numberOfWords; i++) {
            while (true) {
                words[i] = reader.readLine();
                if (isValid(words[i])) {
                    break;
                }
                else {
                    System.out.println("Incorrect word!");
                }
            }
        }

        for (int i = 0; i < numberOfWords; i++) {
            System.out.println(Abbreviation.abbreviationOf(words[i]));
        }
        reader.close();
    }

    private static boolean isValid(String word) {
        Pattern regexp = Pattern.compile("^[a-z]+");
        Matcher m = regexp.matcher(word);
        return (m.matches() && (word.length() < 100));
    }
}
