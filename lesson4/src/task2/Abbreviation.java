package task2;

public class Abbreviation {

    public static String abbreviationOf(String word) {
        if (word.length() > 10) {
            StringBuffer sb = new StringBuffer(word);
            sb.delete(1, word.length() - 1);
            sb.insert(1, String.valueOf(word.length() - 2));
            return sb.toString();
        }
        else {
            return word;
        }
    }
}
