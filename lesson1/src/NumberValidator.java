import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NumberValidator {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String phoneNumber;

        while (true) {
            System.out.print("Enter the phone number: ");
            phoneNumber = reader.readLine();

            if (isMobile(phoneNumber)) {
                StringBuffer tmp = new StringBuffer(phoneNumber);
                tmp.deleteCharAt(0);
                phoneNumber = new String(tmp);

                if ((phoneNumber.matches("[\\d]+")) && (phoneNumber.length() == 12)) {
                    System.out.println("Phone number is correct.");
                    reader.close();
                    break;
                }
                else {
                    System.out.print("Phone number is incorrect. ");
                }
            }
            else {
                System.out.print("Phone number is incorrect. ");
            }
        }

        int sum = digitsSum(Long.parseLong(phoneNumber));
        System.out.println("1st round of calculation, sum is: " + sum );

        while (sum >= 10) {
            sum = digitsSum(sum);
            System.out.println("2st round of calculation, sum is: " + sum );
        }

        switch (sum) {
            case 1:
                System.out.println("Final result is: One");
                break;

            case 2:
                System.out.println("Final result is: Two");
                break;

            case 3:
                System.out.println("Final result is: Three");
                break;

            case 4:
                System.out.println("Final result is: Four");
                break;

            default:
                System.out.println("Final result is: " + sum);
                break;
        }
    }

    private static boolean isMobile(String s) {

        return (s.startsWith("+38067") || s.startsWith("+38068") || s.startsWith("+38096")
                || s.startsWith("+38097") || s.startsWith("+38098") || s.startsWith("+38063")
                || s.startsWith("+38093") || s.startsWith("+38073") || s.startsWith("+38050")
                || s.startsWith("+38066") || s.startsWith("+38095") || s.startsWith("+38099")
                || s.startsWith("+38091") || s.startsWith("+38092") || s.startsWith("+38094"));
    }

    private static int digitsSum(long n) {

        int dsum = 0;
        long x = n;

        while (x != 0) {
            dsum += x % 10;
            x /= 10;
        }
        return dsum;
    }
}
