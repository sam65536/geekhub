package common;

public enum ShapeType {
    CIRCLE, SQUARE, RECTANGLE, TRIANGLE
}
