package common;

public interface Shape {

    double calculateArea();

    double calculatePerimeter();
}
