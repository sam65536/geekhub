import common.*;
import figures.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HomeWork {

    public static void main (String[] args) throws IOException {

        Shape figure;
        ShapeType figureType;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            try {
                System.out.print("Enter the type of figure: ");
                figureType = ShapeType.valueOf(reader.readLine());
                break;
            } catch (Exception e) {
                System.out.println("Wrong type of figure!");
            }
        }

        switch (figureType) {
        case CIRCLE:
            System.out.print("Enter the radius of the Circle: ");
            double radius = Double.parseDouble(reader.readLine());
            figure = new Circle(radius);
            System.out.printf("Area of the Circle is: " + "%.2f%n", figure.calculateArea());
            System.out.printf("Perimeter of the Circle is: " + "%.2f%n", figure.calculatePerimeter());
            reader.close();
            break;

        case SQUARE:
            System.out.print("Enter the value of the side of Square: ");
            double side = Double.parseDouble(reader.readLine());
            figure = new Square(side);
            System.out.printf("Area of the Square is: " + "%.2f%n", figure.calculateArea());
            System.out.printf("Perimeter of the Square is: " + "%.2f%n", figure.calculatePerimeter());
            System.out.print("Square comprises of 2 equivalent Triangles with the next values of sides:\t");
            System.out.printf("%.2f", side);
            System.out.printf("\t" + "%.2f", side);
            System.out.printf("\t" + "%.2f%n", side * Math.sqrt(2));
            reader.close();
            break;

        case RECTANGLE:
            System.out.print("Enter the width of the Rectangle: ");
            double width = Double.parseDouble(reader.readLine());
            System.out.print("Enter the height of the Rectangle: ");
            double height = Double.parseDouble(reader.readLine());
            figure = new Rectangle(width, height);
            System.out.printf("Area of the Rectangle is: " + "%.2f%n", figure.calculateArea());
            System.out.printf("Perimeter of the Rectangle is: " + "%.2f%n", figure.calculatePerimeter());
            double diagonal = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
            System.out.print("Rectangle comprises of 2 equivalent Triangles with the next values of sides:\t");
            System.out.printf("%.2f", width);
            System.out.printf("\t" + "%.2f", height);
            System.out.printf("\t" + "%.2f%n", diagonal);
            reader.close();
            break;

        case TRIANGLE:
            while (true) {
                System.out.print("Enter the value of the first side of Triangle: ");
                double a = Double.parseDouble(reader.readLine());
                System.out.print("Enter the value of the second side of Triangle: ");
                double b = Double.parseDouble(reader.readLine());
                System.out.print("Enter the value of the third side of Triangle: ");
                double c = Double.parseDouble(reader.readLine());

                if (((a + b) <= c) || ((a + c) <= b) || ((b + c) <= a)) {
                    System.out.println("Triangle with such sides does not exist!");
                } else {
                    figure = new Triangle(a, b, c);
                    reader.close();
                    break;
                }
            }
            System.out.printf("Area of the Triangle is: " + "%.2f%n", figure.calculateArea());
            System.out.printf("Perimeter of the Triangle is: " + "%.2f%n", figure.calculatePerimeter());
            break;
        }
    }
}
