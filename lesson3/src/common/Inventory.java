package common;

import java.util.HashMap;
import java.util.Map;

public class Inventory {

    private Map<String, Product> productsMap;

    public Inventory() {
        productsMap = new HashMap<String, Product>();
    }

    public Map<String, Product> getProductsMap() {
        return productsMap;
    }

    public void addToInventory(Product product) {
        productsMap.put(product.getName(), product);
    }

    public double getInventoryValue() {
        double totalPrice = 0;
        for (Product product : productsMap.values()) {
            totalPrice += product.getNetPrice();
        }
        return totalPrice;
    }

    public double getValueOfProductGroup(ProductGroup group) {
        double totalValueOfGroup = 0;
        for (Product product : productsMap.values()) {
            if (product.getGroup().equals(group)) {
                totalValueOfGroup += product.getNetPrice();
            }
        }
        return totalValueOfGroup;
    }
}
