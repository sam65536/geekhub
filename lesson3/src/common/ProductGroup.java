package common;

public enum ProductGroup {
    LAPTOPS, MONITORS, PRINTERS
}
