package products;

import common.Product;
import common.ProductGroup;

public class Laptop extends Product {

    private final ProductGroup group = ProductGroup.LAPTOPS;

    public Laptop(double price, String name, int quantity) {
        super(price, name, quantity);
    }

    @Override
    public ProductGroup getGroup() {
        return group;
    }
}
