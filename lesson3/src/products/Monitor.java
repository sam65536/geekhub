package products;

import common.Product;
import common.ProductGroup;

public class Monitor extends Product {

    private final ProductGroup group = ProductGroup.MONITORS;

    public Monitor(double price, String name, int quantity) {
        super(price, name, quantity);
    }

    @Override
    public ProductGroup getGroup() {
        return group;
    }
}
