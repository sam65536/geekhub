package products;

import common.Product;
import common.ProductGroup;

public class Printer extends Product {

    private final ProductGroup group = ProductGroup.PRINTERS;

    public Printer(double price, String name, int quantity) {
        super(price, name, quantity);
    }

    @Override
    public ProductGroup getGroup() {
        return group;
    }
}
