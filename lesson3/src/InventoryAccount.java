import common.Inventory;
import common.Product;
import common.ProductGroup;
import products.Laptop;
import products.Monitor;
import products.Printer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InventoryAccount {

    public static void main (String args[]) throws IOException {
        Laptop laptop1 = new Laptop(1899.00, "Apple Macbook Pro", 3);
        Monitor monitor1 = new Monitor(119.00, "Acer R240HY", 8);
        Printer printer1 = new Printer(213.00, "HP LaserJet Pro M402dw", 5);

        Inventory inventory = new Inventory();

        inventory.addToInventory(laptop1);
        inventory.addToInventory(monitor1);
        inventory.addToInventory(printer1);

        System.out.println("=============================================CURRENT INVENTORY=============================================");
        for (Product product : inventory.getProductsMap().values()) {
            System.out.println("Product name : " + product.getName() + " | " + "Product price : " + product.getPrice() + " | " + "Product Quantity : " + product.getQuantity() + " | " + "Total Cost : " + (product.getNetPrice()));
        }
        System.out.println("===========================================================================================================");

        System.out.print("Current inventory value: " + inventory.getInventoryValue());
        System.out.println();
        System.out.println("===========================================================================================================");

        for (ProductGroup productgroup : ProductGroup.values()) {
            System.out.println("Current value of " + productgroup + ": " + inventory.getValueOfProductGroup(productgroup));
        }
        System.out.println("===========================================================================================================");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ProductGroup group;

        while (true) {
            System.out.println();
            System.out.print("Do you want to add another product? [y/n]: ");
            String s = reader.readLine();

            if (s.equals("n")) {
                break;
            }
            else if (s.equals("y")) {
                while (true) {
                    try {
                        System.out.print("Enter a group of product: ");
                        group = ProductGroup.valueOf(reader.readLine());
                        break;
                    } catch (Exception e) {
                        System.out.println("Such group of product does not exist...");
                    }
                }
                switch (group) {
                case LAPTOPS:
                    System.out.print("Enter the price of product: ");
                    double LaptopPrice = Double.parseDouble(reader.readLine());
                    System.out.print("Enter the name of product: ");
                    String LaptopName = reader.readLine();
                    System.out.print("Enter the quantity of product: ");
                    int LaptopQuantity = Integer.parseInt(reader.readLine());
                    Laptop laptop = new Laptop(LaptopPrice, LaptopName, LaptopQuantity);
                    inventory.addToInventory(laptop);
                    break;

                case MONITORS:
                    System.out.print("Enter the price of product: ");
                    double MonitorPrice = Double.parseDouble(reader.readLine());
                    System.out.print("Enter the name of product: ");
                    String MonitorName = reader.readLine();
                    System.out.print("Enter the quantity of product: ");
                    int MonitorQuantity = Integer.parseInt(reader.readLine());
                    Monitor monitor = new Monitor(MonitorPrice, MonitorName, MonitorQuantity);
                    inventory.addToInventory(monitor);
                    break;

                case PRINTERS:
                    System.out.print("Enter the price of product: ");
                    double PrinterPrice = Double.parseDouble(reader.readLine());
                    System.out.print("Enter the name of product: ");
                    String PrinterName = reader.readLine();
                    System.out.print("Enter the quantity of product: ");
                    int PrinterQuantity = Integer.parseInt(reader.readLine());
                    Printer printer = new Printer(PrinterPrice, PrinterName, PrinterQuantity);
                    inventory.addToInventory(printer);
                    break;
                }
            }
            else continue;

            System.out.println("=============================================CURRENT INVENTORY=============================================");
            for (Product product : inventory.getProductsMap().values()) {
                System.out.println("Product name : " + product.getName() + " | " + "Product price : " + product.getPrice() + " | " + "Product Quantity : " + product.getQuantity() + " | " + "Total Cost : " + (product.getNetPrice()));
            }
            System.out.println("===========================================================================================================");

            System.out.print("Current inventory value: " + inventory.getInventoryValue());
            System.out.println();
            System.out.println("===========================================================================================================");

            for (ProductGroup productgroup : ProductGroup.values()) {
                System.out.println("Current value of " + productgroup + ": " + inventory.getValueOfProductGroup(productgroup));
            }
            System.out.println("===========================================================================================================");
        }
    reader.close();
    }
}
