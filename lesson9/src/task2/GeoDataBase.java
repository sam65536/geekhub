package task2;

import java.sql.*;

public class GeoDataBase {

    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/geo",
                "app", "p@ssw0rd");
        Statement statement = connection.createStatement();

        System.out.println("TOP 5 countries by the number of regions:");
        ResultSet result1 = statement.executeQuery(
                "SELECT c.name, count(r.id) AS regions " +
                        "FROM country AS c " +
                        "JOIN region AS r ON c.id = r.country_id " +
                        "GROUP BY c.id " +
                        "ORDER BY regions DESC, c.name " +
                        "LIMIT 5");
        while (result1.next()) {
            String country = result1.getString(1);
            int regionCount = result1.getInt(2);
            System.out.println(country + " " + regionCount);
        }
        result1.close();
        System.out.println();

        System.out.println("TOP 5 countries by the number of cities:");
        ResultSet result2 = statement.executeQuery(
                "SELECT c.name, count(r.id) AS cities " +
                        "FROM country AS c " +
                        "JOIN region AS r ON c.id = r.country_id " +
                        "LEFT JOIN city AS ct ON r.id = ct.region_id " +
                        "GROUP BY c.id " +
                        "ORDER BY cities DESC, c.name " +
                        "LIMIT 5");
        while (result2.next()) {
            String country = result2.getString(1);
            int cityCount = result2.getInt(2);
            System.out.println(country + " " + cityCount);
        }
        result2.close();
        System.out.println();

        System.out.println("Countries with the number of regions and cities:");
        ResultSet result3 = statement.executeQuery(
                "SELECT c.name, count(DISTINCT r.id) AS regions, count(r.id) AS cities " +
                        "FROM country AS c " +
                        "JOIN region AS r ON c.id = r.country_id " +
                        "LEFT JOIN city AS ct ON r.id = ct.region_id " +
                        "GROUP BY c.id " +
                        "ORDER BY cities DESC, regions DESC, c.name");
        while (result3.next()) {
            String country = result3.getString(1);
            int regionCount = result3.getInt(2);
            int cityCount = result3.getInt(3);
            System.out.println(country + " " + regionCount + " " + cityCount);
        }
        result3.close();
        statement.close();
        connection.close();
    }
}