package task3;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectH2 {

    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:h2:mem:test", "sa", "");
        Statement statement = connection.createStatement();

        statement.execute("CREATE TABLE IF NOT EXISTS EMPLOYEE (ID INT , NAME VARCHAR )");
        statement.execute("CREATE TABLE IF NOT EXISTS SALARY (ID INT , DATE DATE, VALUE DOUBLE, EMP_ID INT)");

        statement.execute("INSERT INTO EMPLOYEE VALUES(1001, 'Ivanov I.I.')");
        statement.execute("INSERT INTO EMPLOYEE VALUES(1002, 'Petrov P.P.')");
        statement.execute("INSERT INTO EMPLOYEE VALUES(1003, 'Sidorov S.S.')");
        statement.execute("INSERT INTO EMPLOYEE VALUES(1004, 'Andreev A.A.')");
        statement.execute("INSERT INTO EMPLOYEE VALUES(1005, 'Nikolaev N.N.')");

        statement.execute("INSERT INTO SALARY VALUES(1, '2016-10-15', 6287.51, 1001)");
        statement.execute("INSERT INTO SALARY VALUES(2, '2016-10-15', 7368.28, 1002)");
        statement.execute("INSERT INTO SALARY VALUES(3, '2016-10-15', 6574.25, 1003)");
        statement.execute("INSERT INTO SALARY VALUES(4, '2016-10-15', 9310.77, 1004)");
        statement.execute("INSERT INTO SALARY VALUES(5, '2016-10-15', 8144.63, 1005)");
        statement.execute("INSERT INTO SALARY VALUES(6, '2016-11-15', 7252.91, 1001)");
        statement.execute("INSERT INTO SALARY VALUES(7, '2016-11-15', 10640.43, 1002)");
        statement.execute("INSERT INTO SALARY VALUES(8, '2016-11-15', 11970.81, 1003)");
        statement.execute("INSERT INTO SALARY VALUES(9, '2016-11-15', 8562.24, 1004)");
        statement.execute("INSERT INTO SALARY VALUES(10, '2016-11-15', 6931.48, 1005)");
        statement.execute("INSERT INTO SALARY VALUES(11, '2016-12-15', 8529.37, 1001)");
        statement.execute("INSERT INTO SALARY VALUES(12, '2016-12-15', 13272.64, 1002)");
        statement.execute("INSERT INTO SALARY VALUES(13, '2016-12-15', 9674.33, 1003)");
        statement.execute("INSERT INTO SALARY VALUES(14, '2016-12-15', 10089.48, 1004)");
        statement.execute("INSERT INTO SALARY VALUES(15, '2016-12-15', 12874.52, 1005)");

        ResultSet result = statement.executeQuery(
                "SELECT  EMPLOYEE.ID, EMPLOYEE.NAME, SUM(SALARY.VALUE) "
                        + "FROM EMPLOYEE, SALARY "
                        + "WHERE SALARY.EMP_ID = EMPLOYEE.ID "
                        + "GROUP BY EMPLOYEE.ID;");
        while (result.next()) {
            int id = result.getInt(1);
            String name = result.getString(2);
            double sum = result.getDouble(3);
            System.out.println(id + " " + name + " " + sum);
        }
        result.close();
        statement.close();
        connection.close();
    }
}