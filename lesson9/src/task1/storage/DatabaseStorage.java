package task1.storage;

import task1.objects.Entity;
import task1.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link Entity} class.
 * Could be created only with {@link Connection} specified.
 */
public class DatabaseStorage implements Storage {

    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws StorageException {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase() + "s" + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws StorageException {
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase() + "s";
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql));
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws StorageException {
        if (entity.isNew() ) {
            return false;
        }
        Class<?> clazz = entity.getClass();
        String sql = "DELETE FROM " + clazz.getSimpleName().toLowerCase() + "s" + " WHERE id = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            return statement.executeUpdate(sql) > 0;
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws StorageException, IllegalAccessException {
        Map<String, Object> data = prepareEntity(entity);
        String sql = entity.isNew() ? createInsertQuery(entity) : createUpdateQuery(entity);
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql,
                Statement.RETURN_GENERATED_KEYS)) {
            int columnIndex = 1;
            for (Object value : data.values()) {
                preparedStatement.setObject(columnIndex++, value);
            }
            preparedStatement.executeUpdate();
            if (entity.isNew()) {
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    entity.setId(resultSet.getInt(1));
                }
            }
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    // generates INSERT query for PreparedStatement
    private <T extends Entity> String createInsertQuery(T entity) throws IllegalAccessException {
        Map<String, Object> data = prepareEntity(entity);
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO " + entity.getClass().getSimpleName() + "s" + " (");
        for (String column : data.keySet()) {
            sql. append(column + ",");
        }
        sql.deleteCharAt(sql.length() - 1);
        sql.append(") VALUES (");
        for (int i = 0; i < data.size(); i++) {
            sql.append("?,");
        }
        sql.deleteCharAt(sql.length() - 1);
        sql.append(")");
        return sql.toString();
    }

    // generates UPDATE query for PreparedStatement
    private <T extends Entity> String createUpdateQuery(T entity) throws IllegalAccessException {
        Map<String, Object> data = prepareEntity(entity);
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE " + entity.getClass().getSimpleName() + "s" + " SET ");
        for (String column: data.keySet()) {
            sql.append(column + " = ?,");
        }
        sql.deleteCharAt(sql.length() - 1);
        sql.append(" WHERE id = " + entity.getId());
        return sql.toString();
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws IllegalAccessException {
        Map<String, Object> data = new HashMap<>();
        Class<?> clazz = entity.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (!field.isAnnotationPresent(Ignore.class)) {
                field.setAccessible(true);
                data.put(field.getName(), field.get(entity));
            }
        }
        return data;
    }

    //creates list of new instances of clazz by using data from resultSet
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> data = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();
        while (resultSet.next()) {
            T entity = clazz.newInstance();
            entity.setId(resultSet.getInt("id"));
            for (Field field : fields) {
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.setAccessible(true);
                    field.set(entity, resultSet.getObject(field.getName()));
                }
            }
            data.add(entity);
        }
        return data;
    }
}