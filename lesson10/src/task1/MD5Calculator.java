package task1;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Calculator {

    public static String generateMD5(String str) throws NoSuchAlgorithmException {
        byte[] bytes = getBytes(str);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    private static byte[] getBytes(String str) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(str.getBytes());
        byte byteData[] = md.digest();
        return byteData;
    }
}