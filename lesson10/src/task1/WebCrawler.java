package task1;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WebCrawler {

    public static String getContent(URL url) throws IOException {
        StringBuilder sb = new StringBuilder();
        URLConnection uc = url.openConnection();
        try (BufferedReader in = new BufferedReader(new InputStreamReader
                (uc.getInputStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }
        }
        return sb.toString();
    }

    public static void writeToFile(String filename, String data) throws IOException {
        Path p = Paths.get(filename);
        try (OutputStream os = new BufferedOutputStream(
                Files.newOutputStream(p, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
            os.write(data.getBytes(), 0, data.length());
        }
    }
}