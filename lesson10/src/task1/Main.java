package task1;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static task1.MD5Calculator.generateMD5;
import static task1.WebCrawler.getContent;
import static task1.WebCrawler.writeToFile;

public class Main {
    public static void main(String[] args) throws IOException {
        List<String> links = Files.readAllLines(Paths.get("c:\\url.txt"), StandardCharsets.UTF_8);
        ExecutorService taskExecutor = Executors.newFixedThreadPool(links.size());
        for (String link : links) {
            taskExecutor.execute(() -> {
                try {
                    writeToFile("md5.txt", generateMD5(getContent(new URL(link))) + "\n");
                } catch (NoSuchAlgorithmException e) {

                } catch (IOException e) {}
            });
        }
        taskExecutor.shutdown();
    }
}