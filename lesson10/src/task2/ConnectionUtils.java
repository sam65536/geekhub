package task2;

import java.io.*;
import java.net.URL;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    private static final int BUFFER_SIZE = 4096;
    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     *
     * @param url
     * @return
     * @throws java.io.IOException
     */
    public static byte[] getData(URL url) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(BUFFER_SIZE);
        try (InputStream inputStream = new BufferedInputStream(url.openConnection().
                getInputStream(), BUFFER_SIZE)) {
            int currentBytes;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((currentBytes = inputStream.read(buffer, 0, BUFFER_SIZE )) != -1) {
                outputStream.write(buffer, 0, currentBytes);
            }
        }
        return outputStream.toByteArray();
    }
}