import org.junit.Test;

import static org.junit.Assert.assertEquals;
import storage.DatabaseStorageUtils;

public class DatabaseStorageUtilsTest {

    @Test
    public void getTableNameForType() throws Exception {
        assertEquals("users", DatabaseStorageUtils.getTableName(User.class));
    }

    @Test
    public void getTableNameForObject() throws Exception {
        User user = new User();
        assertEquals("users", DatabaseStorageUtils.getTableName(user));
    }
}