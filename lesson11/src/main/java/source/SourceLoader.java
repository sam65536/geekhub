package source;

import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contain all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    SourceProvider sourceProvider;

    public SourceLoader(SourceProvider sourceProvider) {
        this.sourceProvider = sourceProvider;
    }

    public String loadSource(String pathToSource) throws SourceLoadingException {
        if (sourceProvider.isAllowed(pathToSource)) {
            return sourceProvider.load(pathToSource);
        }
        throw new SourceLoadingException();
    }
}