package source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            new URL(pathToSource);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    @Override
    public String load(String pathToSource) throws SourceLoadingException {
        try {
            URL url = new URL(pathToSource);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String currentLine;
            StringBuilder content = new StringBuilder();
            while ((currentLine = reader.readLine()) != null) {
                content.append(currentLine);
            }
            return content.toString();
        } catch (IOException e) {
            throw new SourceLoadingException(e);
        }
    }
}