package source;

import org.junit.Test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class SourceLoaderTest {

    @Test
    public void pathToSourceShouldBeValidatedBeforeLoading () throws Exception {
        SourceProvider mockedProvider = mock(SourceProvider.class);
        SourceLoader sourceLoader = new SourceLoader(mockedProvider);
        when(mockedProvider.isAllowed(anyString())).thenReturn(true);
        sourceLoader.loadSource("currentPath");
        verify(mockedProvider, times(1)).isAllowed("currentPath");
        verify(mockedProvider, never()).isAllowed("anotherPath");
    }
}