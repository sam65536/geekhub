package source;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.*;

public class FileSourceProviderTest {

    @Rule
    public final TemporaryFolder testFolder = new TemporaryFolder();

    @Test
    public void testFilePathValidation() throws Exception {
        File file = testFolder.newFile("file1.txt");
        Path filePath = file.toPath();
        FileSourceProvider fileSourceProvider = new FileSourceProvider();
        assertFalse(fileSourceProvider.isAllowed(""));
        assertTrue(fileSourceProvider.isAllowed(filePath.toString()));
    }

    @Test
    public void testLoadFileSource() throws Exception {
        File file = testFolder.newFile("file2.txt");
        Path filePath = file.toPath();
        Files.write(filePath, "abcdef 1234567890".getBytes());
        FileSourceProvider fileSourceProvider = new FileSourceProvider();
        String content = fileSourceProvider.load(filePath.toString());
        assertEquals(content, "abcdef 1234567890");
        assertNotEquals(content, "abcdef");
    }
}