package source;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.WindowsFakeFileSystem;

import static org.junit.Assert.*;

public class URLSourceProviderTest {

    private static FakeFtpServer fakeFTP = new FakeFtpServer();

    @BeforeClass
    public static void startAndSetUpFakeFTP() {
        UserAccount userAccount = new UserAccount("test", null, "c:\\data");
        userAccount.setAccountRequiredForLogin(false);
        userAccount.setPasswordCheckedDuringValidation(false);
        userAccount.setPasswordRequiredForLogin(false);
        fakeFTP.addUserAccount(userAccount);
        WindowsFakeFileSystem fileSystem = new WindowsFakeFileSystem();
        fileSystem.add(new DirectoryEntry("c:\\data"));
        fileSystem.add(new FileEntry("c:\\data\\file1.txt", "abcdef 1234567890"));
        fakeFTP.setFileSystem(fileSystem);
        fakeFTP.start();
    }

    @AfterClass
    public void tearDown() {
        fakeFTP.stop();
    }

    @Test
    public void testUrlPathValidation() {
        URLSourceProvider urlSourceProvider = new URLSourceProvider();
        assertFalse(urlSourceProvider.isAllowed(""));
        assertTrue(urlSourceProvider.isAllowed("ftp://test@localhost/file1.txt"));
    }

    @Test
    public void testLoadURLSource() throws Exception {
        URLSourceProvider urlSourceProvider = new URLSourceProvider();
        String content = urlSourceProvider.load("ftp://test@localhost/file1.txt");
        assertEquals(content, "abcdef 1234567890");
        assertNotEquals(content, "abcdef");
    }
}