import org.junit.Test;
import source.URLSourceProvider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TranslatorTest {

    @Test
    public void testParseContent() {
        Translator translator = new Translator(new URLSourceProvider());
        String content = "HTTP/1.1 200 OK\n" +
                "Server: nginx\n" +
                "Content-Type: application/xml; charset=utf-8\n" +
                "Content-Length: 68\n" +
                "Connection: keep-alive\n" +
                "Keep-Alive: timeout=120\n" +
                "X-Content-Type-Options: nosniff\n" +
                "Date: Thu, 31 Mar 2016 10:50:20 GMT\n" +
                "\n" +
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<Translation code=\"200\" lang=\"en-ru\">\n" +
                "    <text>Здравствуй, Мир!</text>\n" +
                "</Translation>";

        assertEquals("Здравствуй, Мир!", translator.parseContent(content));
    }

    @Test
    public void testPrepareURL() throws Exception {
        Translator translator = new Translator(new URLSourceProvider());
        String preparedURL = translator.prepareURL("text");
        assertTrue(preparedURL.startsWith("https://translate.yandex.net/api/v1.5/tr/translate?key="));
        assertTrue(preparedURL.endsWith("ru"));
    }
}