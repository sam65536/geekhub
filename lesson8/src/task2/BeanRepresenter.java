package task2;

import task2.test.Ignore;
import task2.test.Car;
import task2.test.Cat;
import task2.test.Human;

import java.lang.reflect.Field;

import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;

public class BeanRepresenter {

    public static void main(String[] args) throws IllegalAccessException {
        Cat cat = new Cat(BLACK, 3, 4, 32);
        Human human = new Human("John", 25, 170, 65);
        Car car = new Car(WHITE, 180, "crossover", "CX-9");
        System.out.println("Cat:");
        representData(cat);
        System.out.println();
        System.out.println("Human:");
        representData(human);
        System.out.println();
        System.out.println("Car:");
        representData(car);
        System.out.println();
    }

    public static void representData(Object o) throws IllegalAccessException {
        Class clazz = o.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Ignore.class)) {
                continue;
            } else {
                field.setAccessible(true);
                Object value = field.get(o);
                System.out.println(field.getName() + " " + value);
            }
        }
    }
}