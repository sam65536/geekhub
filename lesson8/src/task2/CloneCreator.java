package task2;

import task2.test.Human;

import java.lang.reflect.Field;

public class CloneCreator {

    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        Human human = new Human("Jane", 27, 160, 51);
        Human humanClone = (Human) getClone(human);
        System.out.println(humanClone.toString());
    }

    public static Object getClone(Object o) throws IllegalAccessException, InstantiationException, NoSuchFieldException {
        Class clazz = o.getClass();
        Field[] fields = clazz.getDeclaredFields();
        Object clone = clazz.newInstance();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(o);
            Field destinationField = clone.getClass().getDeclaredField(field.getName());
            destinationField.setAccessible(true);
            destinationField.set(clone, value);
        }
        return clone;
    }
}