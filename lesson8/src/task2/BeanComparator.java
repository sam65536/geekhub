package task2;

import task2.test.Car;
import task2.test.Ignore;

import java.lang.reflect.Field;

import static java.awt.Color.BLACK;

public class BeanComparator {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Car car1 = new Car(BLACK, 180, "crossover", "CX-9");
        Car car2 = new Car(BLACK, 260, "crossover", "X5");
        compareData(car1, car2);
    }

    public static <T extends Object> void compareData(T o1, T o2) throws IllegalAccessException, NoSuchFieldException {
        Class clazz = o1.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Ignore.class)) {
                continue;
            } else {
                field.setAccessible(true);
                Object value1 = field.get(o1);
                Object value2 = field.get(o2);
                System.out.println(field.getName() + " " + value1.equals(value2));
            }
        }
    }
}