package task1.json.adapter;

import org.json.JSONArray;
import org.json.JSONException;
import task1.json.JsonSerializer;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {

    @Override
    public Object toJson(Collection c) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        c.forEach(object -> jsonArray.put(JsonSerializer.serialize(object)));
        return jsonArray;
    }
}
