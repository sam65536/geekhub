package translator;

import org.json.JSONObject;
import translator.core.language.Language;
import translator.core.language.LanguageDetector;
import translator.core.language.LanguageDetectorException;
import translator.util.IOUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.URL;

import static com.sun.deploy.net.URLEncoder.encode;

public class YandexLanguageDetector implements LanguageDetector {

    private static final String YANDEX_LANGUAGE_DETECTOR_API_URL =
            "https://translate.yandex.net/api/v1.5/tr.json/detect?key=%s&text=%s";

    private final String apiKey;

    public YandexLanguageDetector(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public Language detect(String text) throws LanguageDetectorException {
        try {
            String request = String.format(YANDEX_LANGUAGE_DETECTOR_API_URL, apiKey, encode(text, "UTF-8"));
            URL url = new URL(request);
            JSONObject jsonObject = getJSONFromResponse(url);
            String langValue = jsonObject.get("lang").toString();
            return Language.find(langValue);
        } catch (Exception e) {
            throw new LanguageDetectorException(e);
        }
    }

    public static JSONObject getJSONFromResponse(URL url) throws IOException {
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        String content = IOUtils.toString(connection.getInputStream());

        return new JSONObject(content);
    }
}
