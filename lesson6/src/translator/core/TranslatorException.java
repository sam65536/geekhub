package translator.core;

public class TranslatorException extends Exception {

    public TranslatorException(Throwable cause) {
        super(cause);
    }
}
