package translator.core.language;

public class LanguageDetectorException extends Exception {

    public LanguageDetectorException(Throwable cause) {
        super(cause);
    }
}
