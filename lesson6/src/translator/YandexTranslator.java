package translator;

import org.json.JSONObject;
import translator.core.Translation;
import translator.core.TranslationRequest;
import translator.core.Translator;
import translator.core.TranslatorException;
import translator.core.language.Language;
import translator.core.language.LanguageDetector;

import java.net.URL;

import static translator.YandexLanguageDetector.*;
import static translator.util.EncodingUtils.encode;

public class YandexTranslator implements Translator {

    private static final String YANDEX_TRANSLATOR_API_URL =
            "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&text=%s&lang=%s";

    private final String apiKey;
    private final LanguageDetector languageDetector;

    public YandexTranslator(String apiKey, LanguageDetector languageDetector) {
        this.apiKey = apiKey;
        this.languageDetector = languageDetector;
    }

    @Override
    public Translation translate(TranslationRequest translationRequest) throws TranslatorException {
        String originalText = translationRequest.getText();
        Language targetLanguage = translationRequest.getTargetLanguage();
        Language originalLanguage;
        String translatedText;

        try {
            originalLanguage = languageDetector.detect(originalText);
            if (originalLanguage.equals(targetLanguage)) {
                return new Translation(originalText, targetLanguage, originalText, targetLanguage);
            }
            String langDirection = prepareLanguageDirection(originalLanguage, targetLanguage);
            String request = String.format(YANDEX_TRANSLATOR_API_URL, apiKey,
                    encode(originalText, "UTF-8"), langDirection);
            URL url = new URL(request);
            JSONObject jsonObject = getJSONFromResponse(url);
            String textValue = jsonObject.get("text").toString();
            translatedText = textValue.substring(2, textValue.length() - 2);
        } catch (Exception e) {
            throw new TranslatorException(e);
        }

        return new Translation(originalText, originalLanguage, translatedText, targetLanguage);
    }

    private String prepareLanguageDirection(Language from, Language to) {
        return from.getCode() + "-" + to.getCode();
    }
}




