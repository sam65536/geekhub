package translator.util;

import java.io.UnsupportedEncodingException;

public class EncodingUtils {

    private EncodingUtils() {
    }

    public static String encode(String text, String charset) throws UnsupportedEncodingException {
        return new String(text.getBytes(), charset);
    }
}
