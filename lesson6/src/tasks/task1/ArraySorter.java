package tasks.task1;

import java.lang.Comparable;
import java.util.Arrays;

import static tasks.task1.Direction.*;

public class ArraySorter {

    public static void main(String[] args) {
       Comparable[] sortedNumbers = BubbleSort(new Integer[]{84, -48, 32, 71, 18, 25, -9, 38, 3, 19}, DESC);
       Comparable[] sortedStrings = InsertionSort(new String[]{"asd", "x", "ws", "qwe", "123", "x1", "jj"}, ASC);
       Comparable[] sortedChars = SelectionSort(new Character[]{'b', '7', 64, 0x6D, 'v', 111, 'n', 0160,}, ASC);

       System.out.println(Arrays.toString(sortedNumbers));
       System.out.println(Arrays.toString(sortedStrings));
       System.out.println(Arrays.toString(sortedChars));
    }

    public static Comparable[] BubbleSort(Comparable[] elements, Direction direction) {
        if (direction == ASC) {
            for (int i = elements.length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (elements[j].compareTo(elements[j + 1]) > 0) {
                        Swap(elements, j, j + 1);
                    }
                }
            }
        }
        else {
            for (int i = elements.length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (elements[j].compareTo(elements[j + 1]) < 0) {
                        Swap(elements, j, j + 1);
                    }
                }
            }
        }

        return elements;
    }

    public static Comparable[] InsertionSort(Comparable[] elements, Direction direction) {
        if (direction == ASC) {
            for (int i = 1; i < elements.length; i++) {
                int j = i;
                while ((j > 0) && (elements[j - 1].compareTo(elements[j]) > 0)) {
                    Swap(elements, j - 1, j);
                    j--;
                }
            }
        }
        else {
            for (int i = 1; i < elements.length; i++) {
                int j = i;
                while ((j > 0) && (elements[j - 1].compareTo(elements[j]) < 0)) {
                    Swap(elements, j - 1, j);
                    j--;
                }
            }
        }

        return elements;
    }

    public static Comparable[] SelectionSort(Comparable[] elements, Direction direction) {
        if (direction == ASC) {
            for (int i = 0; i < elements.length - 1; i++) {
                int index = i;
                for (int j = i + 1; j < elements.length; j++) {
                    if (elements[index].compareTo(elements[j]) > 0) {
                        index = j;
                    }
                }
                Swap(elements, index, i);
            }
        }
        else {
            for (int i = 0; i < elements.length - 1; i++) {
                int index = i;
                for (int j = i + 1; j < elements.length; j++) {
                    if (elements[index].compareTo(elements[j]) < 0) {
                        index = j;
                    }
                }
                Swap(elements, index, i);
            }
        }

        return elements;
    }

    private static void Swap(Comparable[] elements, int i1, int i2) {
        Comparable temp = elements[i1];
        elements[i1] = elements[i2];
        elements[i2] = temp;
    }
}

