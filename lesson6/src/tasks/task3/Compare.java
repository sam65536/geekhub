package tasks.task3;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Compare {
    public static void main(String[] args) {
        final List<Car> cars = Arrays.asList(
                new Car("Volvo", 150000),
                new Car("AUDI", 210000),
                new Car("Volkswagen", 155000),
                new Car("Mercedes", 350000),
                new Car("Honda", 120000),
                new Car("BMW", 400000),
                new Car("Bentley", 500000));

        Collections.sort(cars, (o1, o2) -> {
            if (o1.nameDifference(o2) != 0) {
                return o1.nameDifference(o2);
            } else {
            return o1.getPrice() - o2.getPrice();
            }
        });

        System.out.println(cars.toString());
    }
}