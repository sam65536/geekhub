package tasks.task3;

public class Car {

    private String name;
    private int price;

    public Car(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int nameDifference(Car other) {
        return (name.substring(0, 2)).compareTo(other.getName().substring(0, 2));
    }

    public int getPrice() {
        return price;
    }

    public String toString() {
        return String.format("%s - %d", name, price);
    }
}
