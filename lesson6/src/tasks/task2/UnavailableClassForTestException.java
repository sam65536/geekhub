package tasks.task2;

public class UnavailableClassForTestException extends Exception {

    public UnavailableClassForTestException(Throwable cause) {
        super(cause);
    }

    public UnavailableClassForTestException(String message) {
        super(message);
    }
}
