package tasks.task2;

import java.io.IOException;

public class StringPerformance {

     public static void main(String[] args) throws UnavailableClassForTestException {
     System.out.println("String real time performance: " + getTimePerformance(new String()) + "ms.");
     System.out.println("StringBuffer real time performance: " + getTimePerformance(new StringBuffer()) + "ms.");
     System.out.println("StringBuilder real time performance: " + getTimePerformance(new StringBuilder()) + "ms.");
    }

    private static long getTimePerformance(Object obj) throws UnavailableClassForTestException {

        if (obj instanceof String) {
            long before = System.currentTimeMillis();
            for (int i = 0; i < 20000; i++) {
                obj += "1";
            }
            long after = System.currentTimeMillis();

            return (after - before);
        }
        else if (obj instanceof Appendable) {
            long before = System.currentTimeMillis();
            for (int i = 0; i < 20000; i++) {
                try {
                    ((Appendable) obj).append("1");
                } catch (IOException e) {
                    throw new UnavailableClassForTestException(e);
                }
            }
            long after = System.currentTimeMillis();

            return (after - before);
        }
        else {
            throw new UnavailableClassForTestException("Unsupported type for test performance!");
        }
     }
}
