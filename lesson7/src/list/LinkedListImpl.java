package list;

import java.util.Iterator;

public class LinkedListImpl<E> implements LinkedList<E> {

    private int size = 0;

    private Node<E> tail;
    private Node<E> head;

    @Override
    public void add(E element) {
        //Adds element to the list
        if (tail != null) {
            tail.next = new Node<E>(element, null);
            tail = tail.next;
        } else if (head != null){
            head.next = new Node<E>(element, null);
            tail = head.next;
        } else {
            head = new Node<E>(element, null);
        }
        size++;
    }

    @Override
    public E get(int index) {
        //Return an element for the given index
        //Should throw IndexOutOfBoundsException if index is negative
        // or index is greater then number of elements in the list
        if ((index < 0) || (index >= size)) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        Node<E> ref = head;
        for (int i = 0; i < index; i++) {
            ref = ref.next;
        }
        return ref.value;
    }

    @Override
    public boolean contains(E element) {
        //Checks if an element is present in the list
        Node<E> ref = head;
        while (ref != null) {
            if (ref.value.equals(element)) {
                return true;
            }
        ref = ref.next;
        }
        return false;
    }

    @Override
    public boolean delete(E element) {
        //Deletes element from the list and return true/false if element was removed
        if (head.value.equals(element)) {
            head = head.next;
            size--;
            return true;
        }
        for (Node<E> ref = head; ref != null; ref = ref.next) {
            if (ref.next.value.equals(element)) {
                if (ref.next == tail) {
                    ref.next = null;
                    tail = ref;
                    size--;
                    return true;
                } else {
                    ref.next = ref.next.next;
                    size--;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public E delete(int index) {
        //Deletes element from the list and returns removed element
        //Should throw IndexOutOfBoundsException if index is negative or index is greater then number of elements in the list
        if ((index < 0) || (index >= size)) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        E element = get(index);
        delete(element);
        return element;
    }

    @Override
    public boolean isEmpty() {
        //Checks if the list is empty
        return (size == 0);
    }

    @Override
    public int size() {
        //Returns the size of the list
        return size;
    }

    @Override
    public int clean() {
        //Cleans the list and returns the number of removed elements
        int number = size();
        head = null;
        tail = null;
        size = 0;
        return number;
    }

    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl<>(head);
    }

    private static class IteratorImpl<E> implements Iterator<E> {

        private Node<E> node;

        public IteratorImpl(Node<E> node) {
            this.node = node;
        }

        @Override
        public boolean hasNext() {
            return node != null;
        }

        @Override
        public E next() {
            E value = node.value;
            node = node.next;
            return value;
        }
    }
}