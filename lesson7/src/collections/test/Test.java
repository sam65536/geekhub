package collections.test;

import java.util.Arrays;
import java.util.List;

import static collections.CollectionUtils.*;

public class Test {

    public static void main(String[] args) {
        final List<Person> people = Arrays.asList(
                new Person("John", 20),
                new Person("Sara", 21),
                new Person("Jane", 21),
                new Person("Greg", 35));

        List<Integer> list = Arrays.asList(5, 2, 1, 7, 8, 10, 12, 21, 17, 7, 88, 4, 12, 46, 5, 2);

        System.out.println("RESULT OF FILTER: ");
        System.out.println(filter(list, (e -> e > 10)).toString());

        System.out.println("RESULT OF ANYMATCH: ");
        System.out.println(anyMatch(list, (e -> e > 0)));

        System.out.println("RESULT OF ALLMATCH: ");
        System.out.println(allMatch(list, (integer -> integer > 12)));

        System.out.println("RESULT OF NONEMATCH: ");
        System.out.println(noneMatch(list, (e -> e > 6)));

        System.out.println("RESULT OF MAP: ");
        System.out.println(map(list, (e -> e * 0.5)).toString());

        System.out.println("RESULT OF MAX: ");
        System.out.println(max(list, (e1, e2) -> (e1.compareTo(e2))));

        System.out.println("RESULT OF MIN: ");
        System.out.println(min(list, (e1, e2) -> (e1.compareTo(e2))));

        System.out.println("RESULT OF DISTINCT: ");
        System.out.println(distinct(list));

        System.out.println("RESULT OF FOREACH: ");
        forEach(list, e -> System.out.print(e + " "));
        System.out.println();

        System.out.println("RESULT OF REDUCE 1: ");
        System.out.println(reduce(list, (e1, e2) -> (e1 + e2)));

        System.out.println("RESULT OF REDUCE 2: ");
        System.out.println(reduce(100, list, (e1, e2) -> (e1 > e2) ? e1 : e2));

        System.out.println("RESULT OF PARTITIONING PEOPLE BY AGE > 20: ");
        System.out.println(partitionBy(people, person -> person.getAge() > 20));

        System.out.println("RESULT OF GROUPING PEOPLE BY AGE: ");
        System.out.println(groupBy(people, Person::getAge));

        System.out.println("RESULT OF MAPPING PEOPLE BY AGE AND NAME: ");
        System.out.println(toMap(people, Person::getAge, Person::getName, (s, a) -> s + ", " + a));

        System.out.println("OLDEST PERSON IS: ");
        System.out.println(min(people, (person1, person2) -> person1.ageDifference(person2)));

        System.out.println("YOUNGEST PERSON IS: ");
        System.out.println(max(people, (person1, person2) -> person1.ageDifference(person2)));
    }




}
