package collections;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class CollectionUtils {

    private CollectionUtils() {
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> list = new ArrayList<E>();
        for (E e : elements) {
            if (filter.test(e)) {
                list.add(e);
            }
        }
        return list;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E e : elements) {
            return (predicate.test(e));
        }
        return false;
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E e : elements) {
            if (!predicate.test(e)) {
                return false;
            }
        }
        return true;
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        return allMatch(elements, predicate.negate());
    }

    public static <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        List<R> list = new ArrayList<R>();
        for (T t : elements) {
            list.add(mappingFunction.apply(t));
        }
        return list;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        E max = reduce(elements.get(0), elements, (e1, e2) ->
                (comparator.compare(e1, e2) > 0) ? e1 : e2);
        return Optional.of(max);
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        return max(elements, comparator.reversed());
    }

    public static <E> List<E> distinct(List<E> elements) {
        Set<E> set = new HashSet<E>();
        List<E> list = new ArrayList<E>();
        for (E e : elements) {
            if (set.add(e)) {
                list.add(e);
            }
        }
        return list;
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E e : elements) {
            consumer.accept(e);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        if (elements.size() == 0) {
            return Optional.empty();
        }
        if (elements.size() == 1) {
            return Optional.of(elements.get(0));
        }
        E result = null;
        for (E e : elements) {
            result = (result == null) ? e : accumulator.apply(result, e);
        }
        return Optional.of(result);
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E result = seed;
        for (E e : elements) {
            result = accumulator.apply(result, e);
        }
        return result;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        Map<Boolean, List<E>> map = new HashMap<Boolean, List<E>>();
        for (E e : elements) {
            boolean key = predicate.test(e);
            map.putIfAbsent(key, new ArrayList<E>());
            map.replace(key, map.get(key)).add(e);
        }
        return map;
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        Map<K, List<T>> map = new HashMap<K, List<T>>();
        for (T t : elements){
            K key = classifier.apply(t);
            map.putIfAbsent(key, new ArrayList<T>());
            map.replace(key, map.get(key)).add(t);
        }
        return map;
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction) {

        Map<K, U> map = new HashMap<K, U>();
        for (T t : elements){
            map.merge(keyFunction.apply(t), valueFunction.apply(t), mergeFunction);
        }
        return map;
    }
}