package task1.source;

public class SourceLoadingException extends Exception {

    public SourceLoadingException() {
    }

    public SourceLoadingException(String message) {
        super(message);
    }

    public SourceLoadingException(Throwable cause) {
        super(cause);
    }
}