package task1;

public class TranslateException extends Exception {

    public TranslateException() {
    }

    public TranslateException(String message) {
        super(message);
    }

    public TranslateException(Throwable cause) {
        super(cause);
    }
}
