package task2.common;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface FolderViewer {

public final String[] AUDIO_FILE_EXTENSIONS = {"mp3", "wav", "wma"};

public final String[] VIDEO_FILE_EXTENSIONS = {"avi", "mp4", "flv"};

public final String[] IMAGE_FILE_EXTENSIONS = {"jpeg", "jpg", "png", "gif"};

public String getFileExtension(File file);

public FileType getFileType(File file) throws UnknownFileTypeException;

public List<File> getAllFiles(File folder);

public Map<FileType, List<File>> getAllFilesByType(File folder);

}
