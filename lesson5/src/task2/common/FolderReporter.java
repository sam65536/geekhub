package task2.common;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FolderReporter implements FolderViewer {

    private Map<FileType, List<File>> filesByType = new HashMap<>();

    public void add(FileType type, File file) {
        List<File> files = this.filesByType.get(type);

        if (files == null) {
            files = new ArrayList<>();
            filesByType.put(type, files);
        }

        files.add(file);
    }

    @Override
    public String getFileExtension(File file) {
        if (file.isDirectory()) {
            return null;
        }
        String fileName = file.getName();
        int index = fileName.lastIndexOf(".");

        return  fileName.substring(index + 1).toLowerCase();
    }

    @Override
    public FileType getFileType(File file) throws UnknownFileTypeException {
        String ext = getFileExtension(file);

        for (int i = 0; i < AUDIO_FILE_EXTENSIONS.length; i++) {
            if (AUDIO_FILE_EXTENSIONS[i].equals(ext)) {
                return FileType.AUDIO;
            }
        }
        for (int i = 0; i < VIDEO_FILE_EXTENSIONS.length; i++) {
            if (VIDEO_FILE_EXTENSIONS[i].equals(ext)) {
                return FileType.VIDEO;
            }
        }
        for (int i = 0; i < IMAGE_FILE_EXTENSIONS.length; i++) {
            if (IMAGE_FILE_EXTENSIONS[i].equals(ext)) {
                return FileType.IMAGE;
            }
        }
        throw new UnknownFileTypeException("Unsupported file type!");
    }

    @Override
    public List<File> getAllFiles(File folder) {
        List<File> files = new ArrayList<File>();

        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                files.addAll(getAllFiles(file));
            }
            else {
                files.add(file);
            }
        }
        return files;
    }

    @Override
    public Map<FileType, List<File>> getAllFilesByType(File folder) {

        for (File file : getAllFiles(folder)) {
            try {
               FileType fileType = getFileType(file);
                switch (fileType) {
                    case AUDIO:
                        add(FileType.AUDIO, file);
                        break;

                    case VIDEO:
                        add(FileType.VIDEO, file);
                        break;

                    case IMAGE:
                        add(FileType.IMAGE, file);
                        break;
                }
            } catch (UnknownFileTypeException e) {}
        }
        return filesByType;
    }
}
