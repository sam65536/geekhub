package task2.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipMaker {

    public static void zipFiles (List<File> sourceFiles, String zipFileName) throws IOException {

        try (FileOutputStream outputStream = new FileOutputStream(zipFileName);
             ZipOutputStream zipOut = new ZipOutputStream(outputStream)) {

            for (File sourceFile : sourceFiles) {

                FileInputStream inputStream = new FileInputStream(sourceFile);
                ZipEntry zipEntry = new ZipEntry(sourceFile.getName());
                zipOut.putNextEntry(zipEntry);

                byte[] bytes = new byte[1024];
                int length;

                while ((length = inputStream.read(bytes)) >= 0) {
                    zipOut.write(bytes, 0, length);
                }

                inputStream.close();
            }
        }
    }
}
