package task2.common;

public class UnknownFileTypeException extends Exception {

    public UnknownFileTypeException() {
    }

    public UnknownFileTypeException(String message) {
        super(message);
    }

    public UnknownFileTypeException(Throwable cause) {
        super(cause);
    }
}
