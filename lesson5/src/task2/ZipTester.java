package task2;

import task2.common.FileType;
import task2.common.FolderReporter;
import task2.common.ZipMaker;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ZipTester {

    public static void main(String[] args) {

        FolderReporter reporter = new FolderReporter();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a path to a directory: ");
        String path = scanner.next();

        while (true) {
            try {
                Map<FileType, List<File>> filesByType = reporter.getAllFilesByType(new File(path));
                for (FileType type : FileType.values()) {
                    if (filesByType.get(type) != null) {
                        ZipMaker.zipFiles(filesByType.get(type), type.toString().toLowerCase() + "s.zip");
                    }
                }
                break;
            } catch (Exception e) {
                System.out.print("Enter another path: ");
                path = scanner.next();
            }
        }
    }
}

