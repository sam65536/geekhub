import objects.Entity;
import objects.Ignore;

import java.time.LocalDate;

public class User extends Entity {

    private String name;
    private Integer age;
    private Boolean admin;
    private Double balance;

    @Ignore
    private LocalDate creationDate;

    public User() {
        this.creationDate = LocalDate.now();
    }

    public User(String name, Integer age, Boolean admin, Double balance) {
        this.creationDate = LocalDate.now();
        this.name = name;
        this.age = age;
        this.admin = admin;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
        {
            return false;
        }
        if (o == this)
        {
            return true;
        }
        if (getClass() != o.getClass())
        {
            return false;
        }

        User user = (User) o;

        return (this.getName().equals(user.getName()))
                && (this.getAge() == user.getAge())
                && (this.getAdmin() == user.getAdmin())
                && (this.getBalance().equals(user.getBalance()));
    }
}