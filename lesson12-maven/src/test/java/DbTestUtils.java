import java.sql.Connection;
import java.sql.Statement;

public class DbTestUtils {

    public static void execute(Connection connection, String sql) throws Exception {
        Statement statement = connection.createStatement();
        statement.execute(sql);
    }
}
