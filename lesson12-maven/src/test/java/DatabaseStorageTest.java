import org.junit.*;
import storage.DatabaseStorage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import static org.junit.Assert.*;

public class DatabaseStorageTest {

    private static final String CONNECTION_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String USER = "sa";
    private static final String PASSWORD = "";
    private static final String Schema = "CREATE TABLE users (\n" +
            "  id INT AUTO_INCREMENT PRIMARY KEY,\n" +
            "  name VARCHAR(255),\n" +
            "  age INTEGER,\n" +
            "  admin BOOLEAN,\n" +
            "  balance DOUBLE\n" +
            ");";

    private static Connection connection;

    private DatabaseStorage databaseStorage = new DatabaseStorage(connection);

    @BeforeClass
    public static void init() throws Exception {
        connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
    }

    @Before
    public void createSchema() throws Exception {
        DbTestUtils.execute(connection, Schema);
        DbTestUtils.execute(connection,
                "INSERT INTO users(name, age, admin, balance)\n" +
                        "VALUES ('Victor', 23, true, 22.23)");
        DbTestUtils.execute(connection,
                "INSERT INTO users(name, age, admin, balance)\n" +
                        "VALUES ('John', 20, FALSE, 32.83)");
        DbTestUtils.execute(connection,
                "INSERT INTO users(name, age, admin, balance)\n" +
                        "VALUES ('Sara', 21, FALSE, 17.31)");
        DbTestUtils.execute(connection,
                "INSERT INTO users(name, age, admin, balance)\n" +
                        "VALUES ('Greg', 35, TRUE, 48.64)");
    }

    @After
    public void tearDown() throws Exception {
        DbTestUtils.execute(connection, "DROP TABLE users");
    }

    @Test
    public void get() throws Exception {
        User user1 = new User("Victor", 23, true, 22.23);
        assertEquals(user1, databaseStorage.get(User.class, 1));
        assertNull(databaseStorage.get(User.class, 5));
    }

    @Test
    public void list() throws Exception {
        User user1 = new User("Victor", 23, true, 22.23);
        User user2 = new User("John", 20, false, 32.83);
        User user3 = new User("Sara", 21, false, 17.31);
        User user4 = new User("Greg", 35, true, 48.64);

        List<User> usersList = databaseStorage.list(User.class);

        assertEquals(user1, usersList.get(0));
        assertEquals(user2, usersList.get(1));
        assertEquals(user3, usersList.get(2));
        assertEquals(user4, usersList.get(3));
    }

    @Test
    public void testDeleteAll() throws Exception {
        assertEquals(4, databaseStorage.delete(User.class));
        assertEquals(0, databaseStorage.count(User.class));
    }

    @Test
    public void save() throws Exception {
        User user5 = new User("Bob", 25, false, 24.75);
        databaseStorage.save(user5);
        assertEquals(user5, databaseStorage.get(User.class, 5));

        User user6 = new User("Jane", 22, true, 18.51);
        user6.setId(2);
        databaseStorage.save(user6);
        assertEquals(user6, databaseStorage.get(User.class, 2));
    }

    @Test
    public void testDeleteOne() throws Exception {
        assertFalse(databaseStorage.delete(User.class, 5));
        assertTrue(databaseStorage.delete(User.class, 1));
        assertEquals(3, databaseStorage.count(User.class));
    }
}