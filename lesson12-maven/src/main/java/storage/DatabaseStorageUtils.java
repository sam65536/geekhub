package storage;

import objects.Entity;

public class DatabaseStorageUtils {

    private DatabaseStorageUtils() {
    }

    public static <T extends Entity> String getTableName(Class<T> type) {
        return type.getSimpleName().toLowerCase() + "s";
    }

    public static <T extends Entity> String getTableName(T entity) {
        return getTableName(entity.getClass());
    }
}